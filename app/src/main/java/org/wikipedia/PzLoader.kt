package org.wikipedia

import android.content.ClipDescription
import android.content.ClipboardManager
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Build
import android.text.InputType
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.pointzi.Pointzi
import com.pointzi.Pointzi.state
import org.wikipedia.main.MainActivity
import org.wikipedia.settings.Prefs
import org.wikipedia.settings.PrefsIoUtil
import org.wikipedia.settings.PrefsIoUtil.setString
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

@Suppress("ConstantConditionIf")
class PzLoader(val activity: MainActivity) {
    private lateinit var pointziAppKey: String

    init {

        val flavor = BuildConfig.FLAVOR

        // AppKey

        var appKey: String? = null

        when {
            flavor.contains("Zebra") -> {
                appKey = "PZDynamic_zebra"
            }
            flavor.contains("Bison") -> {
                appKey = "PZDynamic_bison"
            }
            flavor.contains("BDD") -> {
                appKey = "wikipedia_bdd"
            }
            flavor.contains("Demo") -> {
                appKey = "wikipedia_demo"
            }
            flavor == "dev" -> {
                // appKey = "PZDynamic_zebra"
                // appKey = "PZDynamic_bison"
                appKey = "capivara"
                // appKey = "rn_zebra"
            }
        }

        setString("APP_KEY", appKey)

        if (appKey == null) {
            showAppKeyDialog()
        } else {
            // Init

            Pointzi.init(activity.application, appKey)
            setUserId()
        }

        // Debug Level
        Pointzi.setLogLevel("DEBUG")

        // OD button
        activity.onDemandButton.setOnClickListener { showOnDemandDialog() }
    }

    //region POINTZI DIALOGS

    private fun showOnDemandDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Please enter the campaignId")
        // Set up the input
        val input = EditText(activity)
        input.inputType = InputType.TYPE_CLASS_TEXT
        builder.setView(input)
        builder.setNeutralButton(
            "Paste"
        ) { _: DialogInterface?, _: Int -> }
        // Set up the buttons
        builder.setPositiveButton(
            "Run"
        ) { _: DialogInterface?, _: Int ->
            val campaignId = input.text.toString()
            Pointzi.showOnDemandCampaign(campaignId)
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog: DialogInterface, _: Int -> dialog.cancel() }
        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEUTRAL)
            .setOnClickListener {
                try {
                    val clipboard = activity.getSystemService(
                        Context.CLIPBOARD_SERVICE
                    ) as ClipboardManager?
                    // If it does contain data, decide if you can handle the data.
                    if (clipboard != null) { // since the clipboard has data but it is not plain text
                        if (clipboard.hasPrimaryClip() &&
                            clipboard.primaryClipDescription!!
                                .hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)
                        ) { // since the clipboard contains plain text.
                            val item = clipboard.primaryClip!!
                                .getItemAt(0)
                            // Gets the clipboard as text.
                            input.setText(item.text.toString())
                        }
                    }
                } catch (ignored: Throwable) {
                }
            }
    }

    private fun showAppKeyDialog() {
        val builder =
            AlertDialog.Builder(activity)
        builder.setTitle("Please enter you AppKey")
        // Set up the input
        val input = EditText(activity)
        // Specify the type of input expected; this, for example, sets the
// input as a password, and will mask the text
        input.inputType = InputType.TYPE_CLASS_TEXT
        builder.setView(input)
        // Set up the buttons
        builder.setPositiveButton(
            "Enter"
        ) { _: DialogInterface?, _: Int ->
            pointziAppKey = input.text.toString()
            Pointzi.init(activity.application, pointziAppKey)
            setUserId()
            val e: SharedPreferences.Editor = activity.getSharedPreferences(
                "wiki",
                Context.MODE_PRIVATE
            ).edit()
            e.putString("APP_KEY", pointziAppKey)
            e.commit()
        }
        builder.setNegativeButton(
            "Default"
        ) { dialog: DialogInterface, _: Int -> dialog.cancel() }
        builder.show()
    }

    private fun setUserId() {
        if (state.app.key.get().isNotEmpty()) {
            val df: DateFormat = SimpleDateFormat("EEE, d MMM yyyy, HH:mm", Locale.US)
            val date: String = df.format(Calendar.getInstance().time)
            val userId: String
            val model: String = if (Build.MODEL.length > 64) {
                Build.MODEL.substring(0, 63)
            } else {
                Build.MODEL
            }
            userId = if (BuildConfig.FLAVOR == "dev") {
                String.format("Capi (%s)%s", model, date)
            } else {
                "$model $date"
            }
            Pointzi.setUserId(userId)
        }
    }

    companion object {
        //endregion
        fun disableOnboarding() { // main onboarding screen
            Prefs.setInitialOnboardingEnabled(false)
            // onboarding feed cards
            PrefsIoUtil.setBoolean(
                R.string.preference_key_feed_customize_onboarding_card_enabled,
                false
            )
        }
    }
}
